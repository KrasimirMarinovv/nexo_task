# nexo_task

This repository has the purpose of doing a technical task for nexo. You can find the following things in it:

#hello_world.py

Python application withthe purpose of sening "Hello World" every 1 second.

#Dockerfile

Docker file with the purpose of building a docker image for the python application.

#docker-compose.yml

Tried to start a local environment with docker-compose, which includes the image built in the previous step, 
as well as a few other images like ubuntu18.04, apline3.13 and one mysql.

#gitlab-ci.yml

Defines a pipeline, which consist of 2 stages - buld and push. The purpose is to build a new image, every time a new commit to the repository is made. However, currently is has some issue with the "docker push" command in the build stage, which at this time I am unable to resolve.

#Other yaml files

The rest of the yaml files were created after running the "kompose convert" command in order to convert the docker-compose.yml file to files that can be used with kubectl and minikube for k8s deployment.
